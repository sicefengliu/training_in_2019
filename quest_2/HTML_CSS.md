# HTML + CSS

### 任务信息
|序号|预计用时|难度|Deadline|备注|
|:--:|:-----:|:-:|:-------:|:-:|
|2|1 - 2 day(s)|中等|没有（大概）|-|
## 准备
- 浏览器
- 文本编辑器/IDE
- 新的码云仓库，统一命名`sice_fengliu_2019_quest_2`
## 任务目标
- 自行学习下述部分并理解其在浏览器中的作用，自寻教程（为建立概念，建议通读[MDN-建立你的第一个网站](https://developer.mozilla.org/zh-CN/docs/Learn/Getting_started_with_the_web)，并可以尝试从[MDN-你的网站看起来是什么样的？](https://developer.mozilla.org/zh-CN/docs/Learn/Getting_started_with_the_web/What_will_your_website_look_like)开始跟随教程做一遍）：
	- HTML，着重了解：
		- 表单标签
	- CSS，着重了解：
		- 盒模型
		- 布局

_***注意：第一节安装基础软件已经给出要求，不再推荐阅读；JavaScript本节中不涉及但很快会用到，因此建议尽快入门**_
- 在完成学习任务后，完成以下页面（希望你能利用好`git`，将本地的开发文件与远程仓库`sice_fengliu_2019_quest_2`同步）：
	- 用户注册页面，其中用户可填写的字段包括但不限于：
		- 用户ID
		- 电子邮箱
		- 手机号码
		- 固定电话
		- 密码
		- 重复密码
		- 以及两个按钮
		_分别用于提交注册结果和取消（清空已填写信息）_
	- 用户登陆页面，其中用户可填写的字段包括但不限于：
		- 用户ID/E-mail/Tel/Mobile四合一字段
		_目的是为了让用户填写任意字段即可验明正身_
		- 密码
		- 验证码，旁边附有验证码图片框（可选）
		- 以及两个按钮
		_分别用于提交注册结果和取消（清空已填写信息）_
	- 登录成功页面，显示登陆成功即可，若脑内已有Web应用的雏形，也可依照具体需要添加字段
_***注意：以上描述仅为提示，此次均不用实现具体功能，具体功能在下一期进行**_
- 使用Gitee Pages服务，托管你的网页
（[这是官方文档](https://gitee.com/help/articles/4136)，理念和上述教程中发布网站一节中的[通过GitHub发布](https://developer.mozilla.org/zh-CN/docs/Learn/Getting_started_with_the_web/Publishing_your_website)类似）
- 在仓库内编写`Markdown`文档，描述你的学习经历，说明你的项目结构，完成情况与特色；
- 老生常谈的内容，将你的项目文件push到Gitee上的仓库`sice_fengliu_2019_quest_2`中，修改你fork的仓库`2019年度培训`中`README.md`文档，添加你的姓名（名用*隐去），学号，`Markdown`文档链接，你的网页的链接；
- __基础要求__：
	- 文件目录结构清晰，HTML与CSS样式分离
	- 页面设计简介，页面元素尺寸恰当搭配合理，如字体大小要合适
	- 要注重用户体验，表单要带有提示信息，如placeholder可显示示例字段，input上下方进行描述，旁边使用文字或图标示意字段含义（参见下面的示意图）
- __进阶要求__：
	- 简单页面自适应：可简单提供两个CSS文件，利用CSS3的media query模块
	- 利用CSS伪类和过渡，实现简单动画效果
	- 其他自选
```
            /--------------------------------------------\
            |            ____________________________    |
            |   E-mail: |_example@xxx.com____________|   |
            |             Please enter your E-mail.      |
            |            ____________________________    |
            | Password: |_******_____________________|   |
            |             Please enter your password.    |
            |                                            |
            |        _________           ________        |
            |       |_confirm_|         |_cancel_|       |
            \--------------------------------------------/
```
_这只是为了告诉你们什么是placeholder_
