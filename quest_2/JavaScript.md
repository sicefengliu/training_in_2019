# JavaScript
### 任务信息

| 序号 |   预计用时   | 难度 |     Deadline     | 备注 |
| :--: | :----------: | :--: | :--------------: | :--: |
|  3   | 1 - 2 day(s) | 中等 | 十一月初（大概） |  -   |

**考虑到导论课中已经引入微信小程序，为了减轻各位的负担，本次作业不但可以编写H5网页，也可以编写小程序**

## 准备

- 浏览器
- 文本编辑器/IDE
- 新的码云仓库，统一命名`sice_fengliu_2019_quest_3`（请把所有的文件都放在这里，别放到`training_in_2019`这个仓库里）

_**注意：第一节安装基础软件已经给出要求，不再推荐阅读**_

## 学习目标

- 请先学习`JavaScript`相关知识（可参考[MDN-JavaScript基础](https://developer.mozilla.org/zh-CN/docs/Learn/Getting_started_with_the_web/Javascript_basics)，[菜鸟教程-JavaScript 教程](https://www.runoob.com/js/js-tutorial.html)），语法上不用太深入，但要能够明白如何操作网页
- 如果你要开发`H5`，请学习` HTML DOM `（这也包含在上面的两个教程中）
- 如果你要开发`小程序`，请先阅读[微信开发文档](https://developers.weixin.qq.com/miniprogram/dev/framework/)
- 了解正则表达式的基础（主要用于格式判断）

## 作业目标

- 注册界面
  - 用户名需要检查是否已被使用
  - 邮箱需要检查格式是否正确
  - 密码需要检查复杂度和两次密码输入是否一致
  - 注册成功时需要把用户注册信息存放至`localStorage`（无需加密处理）
  - 其他内容（比如用户名长度检查）
- 登录页面
  -  登录时检查填写的账户信息是否正确并给出反馈（错误弹窗以及成功跳转） 
- 登录成功页面
  - 在页面中显示用户的基本信息， 包括但不限于昵称、邮箱、注册时间、上次登录时间
- 使用`Gitee Pages`服务，托管你的网页（开发微信小程序的不用考虑）
- 将你的学习经历，项目完成情况，特色，心得体会等写到仓库中的` Readme.md `文档中
- 将你的项目文件push到Gitee上的仓库`sice_fengliu_2019_quest_3`中，修改你fork的仓库`2019年度培训`中`README.md`文档，添加你的姓名（名用*隐去），学号，`Markdown`文档链接，你的网页的链接
- __基础要求__：
	- 文件目录结构清晰，`html`、`css`、`js` 文件相互分离
	- 页面设计合理，各个元素大小、位置摆放正常，符合常规用户习惯
	- 有受众意识，要能够确保用户的体验良好
- __进阶要求__：
	- 界面的美化，比如添加一些动画效果，注重各个元素的间距、位置
	  _**提示：可以使用各种 UI 框架**_
	- 页面跳转：① 未登录时直接访问登录成功页面将跳转至登录页面；② 注册成功后直接进入登录成功页面；③ 登录页面有注销按钮，点击后将跳转回登录页 
	- 统计用户登陆次数，并在登录成功页面中显示
	- 验证码功能，使用纯`JS`验证
	- 记住密码功能 
	- 其他自选

## 参考网页

- [简书](https://www.jianshu.com/sign_up)
- [DeviantArt](https://www.deviantart.com/join/)
- [Google](https://accounts.google.com/signup/v2/webcreateaccount?service=youtube&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Faction_handle_signin%3Dtrue%26app%3Ddesktop%26hl%3Dzh-CN%26next%3D%252F&hl=zh-CN&dsh=S-955977561%3A1571068251712827&gmb=exp&biz=false&flowName=GlifWebSignIn&flowEntry=SignUp&nogm=true)
- [加勒里克斯在线博物馆](https://gallerix.asia/user/join/)
- [Pinterest](https://www.pinterest.com/)（注意其在PC端和移动端的不同）
- [abletive](http://www.abletive.com/register)
